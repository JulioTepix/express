module.exports = (sequelize, Sequelize) => {
    const Product = sequelize.define("products", {
      precio: {
        type: Sequelize.STRING
      },
      nombre: {
        type: Sequelize.STRING
      },
      modelo: {
        type: Sequelize.STRING
      },
      tipo: {
        type: Sequelize.STRING
      }
    });
  
    return Product;
  };