const db = require("../models");
const Product = db.product;

console.log(db);
  
  exports.add = async (req, res) => {
    try {
        const product = await Product.create({
          precio: req.body.precio,
          nombre: req.body.nombre,
          modelo: req.body.modelo,
          tipo: req.body.tipo,
        });
    
        if (product) {

          res.send({ message: "Product registered successfully!" });
        } else {
          res.send({ message: "Product registered successfully!" });
        }
      } catch (error) {
        res.status(500).send({ message: error.message });
      }
  };
  
  exports.allproducts = async (req, res) => {
    const product = await Product.findAll();
    res.status(200).send(product);
  };

  exports.delete = async (req, res) => {
    Product.destroy({
        where: {
          id: req.body.id
        }
      }).then(() => {
        res.send({ message: "Product deleted successfully!" });
      }).catch(err => {
        res.status(500).send({ message: err.message });
      });
  };