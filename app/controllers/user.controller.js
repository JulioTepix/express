const db = require("../models");
const User = db.user;

console.log(db);

exports.allAccess = (req, res) => {
    res.status(200).send("Public Content.");
  };
  
  exports.userBoard = (req, res) => {
    res.status(200).send("User Content.");
  };
  
  exports.adminBoard = async (req, res) => {
    const user = await User.findAll();
    res.status(200).send(user);
  };

  exports.signup = async (req, res) => {
    const user = await User.findAll();
    res.status(200).send("Admin Content.");
  }